const num = 0;
let temp = 1;

function sqrt(num) {
    let iteration = 1;

    do {
        temp = iteration;
        iteration = (temp + (num/temp))/2;
    } while (temp - iteration != 0);

    document.write("The square root of the number is: " + iteration.toFixed(10));
}

const userinput = prompt("This program finds the square root of the number entered by the user. Enter any number: ", "0");
sqrt(userinput)