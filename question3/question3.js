const num = 0;
let factor = "";
let x = 0;

function primeFactors(num) {
    for (x = 2; x < num; x++) {
        while (num%x == 0) {
            factor = factor + x;
            num = num/x;
            if (num >= x) {
                factor = factor + ' X ';
            }
        }
    }

    if (num != 1) {
        factor = factor + num;
    }

    document.write("Your number's prime factors are: " + factor);
    factor = '';
}

const userinput = prompt("This program finds the prime factors of the number entered by the user. Enter any number: ", "0");
primeFactors(userinput)